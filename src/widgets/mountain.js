var $ = require('jquery');

function Mountain(node){

    this.$node = $(node);
    this.$window = $(window);

    this.$window.on('load', $.proxy(this.mountainLoad, this));

}

Mountain.prototype.mountainLoad = function(elem){
    this.$node.children().addClass('active');
}

module.exports = Mountain;
