var $ = require('jquery');

var widgets;

widgets = {
    'mountain' : require('./widgets/mountain.js')
}

$(document).ready(function(){
    var $binder = $('.widget');
    $binder.each(function(){
        var e = $(this).attr('data-widget');
        new widgets[e](this);
    });
});
